from enum import Enum
from math import sin, cos, \
    sqrt, factorial


class Action(Enum):
    ADD = 0,
    # +
    SUBTRACT = 1,
    # -
    MULTIPLY = 2,
    # *
    DIVIDE = 3,
    # /
    MODULO = 4,
    # %
    DIVIDE_WITHOUT_REMAINDER = 5,
    # //
    POW = 6,
    # pow()
    SIN = 7,
    # math.sin()
    COS = 8,
    # math.cos()
    SQRT = 9,
    # math.sqrt()
    FACTORIAL = 10,
    # math.factorial()
    WITH_PARALLELEPIPED = 11
    # link to lambda expressions
    # with parallelepiped


class Functionality:

    # FIRST FUNCTION #
    @staticmethod
    def calculator(action, a, b=None,
                   h=None, is_p=None):
        match action:
            case Action.ADD:
                return a + b
            case Action.SUBTRACT:
                return a - b
            case Action.MULTIPLY:
                return a * b
            case Action.DIVIDE:
                return a / b
            case Action.MODULO:
                return a % b
            case Action.DIVIDE_WITHOUT_REMAINDER:
                return a // b
            case Action.POW:
                return pow(a, b)
            case Action.SIN:
                return sin(a)
            case Action.COS:
                return cos(a)
            case Action.SQRT:
                return sqrt(a)
            case Action.FACTORIAL:
                return factorial(a)
            case Action.WITH_PARALLELEPIPED:
                if is_p:
                    return (lambda width, length, height:
                            4 * (width + length + height))(a, b, h)
                else:
                    return (lambda width, length, height:
                            2 * (width * length +
                                 width * height +
                                 length * height))(a, b, h)
        print('not correct input data')

    # SECOND FUNCTION #
    @staticmethod
    def count_in_line():
        symbols_count = spaces_count \
            = comma_count = 0
        print('enter the line: ', end='')
        line = input()
        for char in line:
            if char.isspace():
                spaces_count += 1
            elif char == ',':
                comma_count += 1
                symbols_count += 1
            elif char.isascii():
                symbols_count += 1

        print('count of symbols = ', symbols_count)
        print('count of spaces = ', spaces_count)
        print('count of commas = ', comma_count)

    # THIRD FUNCTION #
    @staticmethod
    def matrix(count_columns, count_rows, begin, step):
        print('matrix')

        cell = begin

        # initializing of array
        arr = [0] * count_columns
        for i in range(count_columns):
            arr[i] = [0] * count_rows

        for i in range(count_columns):
            for j in range(count_rows):
                arr[i][j] = cell
                cell += step
                print(arr[i][j], end=' ')
            print()
