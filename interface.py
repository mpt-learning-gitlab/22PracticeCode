from enum import Enum
from functionality \
    import Action, Functionality


class Position(Enum):
    INSIDE = 1,
    OUTSIDE = 2


class Interface:

    @staticmethod
    def input_float(value):
        while True:
            try:
                value = float(value)
                return value
            except ValueError:
                print('no float or int,'
                      ' repeat input: ',
                      end='')
            value = input()

    @staticmethod
    def input_int(value):
        while True:
            try:
                value = int(value)
                return value
            except ValueError:
                print('no int,'
                      ' repeat input: ',
                      end='')
            value = input()

    @classmethod
    def user_input(cls, count_of_values):
        int(count_of_values)
        if count_of_values >= 1 & count_of_values <= 3:
            print('enter first number: ', end='')
            first_number = cls.input_float(input())
            if count_of_values >= 2:
                print('enter second number: ', end='')
                second_number = cls.input_float(input())
                if count_of_values == 3:
                    print('enter third number: ', end='')
                    third_number = cls.input_float(input())
                    return first_number, second_number, third_number
                else:
                    return first_number, second_number
            else:
                return first_number

    @classmethod
    def back(cls, in_calculator=False):
        if in_calculator:
            cls.calculator_outside_menu()
        else:
            cls.menu()

    @staticmethod
    def guide(is_main_menu, calculator_position=None):
        if is_main_menu:
            print('-- functions --\n'
                  '0 - exit\n'
                  '- by assignment -\n'
                  '1 - calculator\n'
                  '2 - count in line\n'
                  '3 - matrix\n'
                  '- other -\n'
                  '4 - guide')

        elif calculator_position == Position.OUTSIDE:
            print('-- functions --\n'
                  '0 - back\n'
                  '1 - guide\n'
                  '-- operations --\n'
                  '2 - add\n'
                  '3 - subtract\n'
                  '4 - multiply\n'
                  '5 - divide\n'
                  '6 - modulo\n'
                  '7 - divide'
                  ' without remainder\n'
                  '8 - pow\n'
                  '9 - sin\n'
                  '10 - cos\n'
                  '11 - sqrt\n'
                  '12 - factorial\n'
                  '13 - perimeter'
                  ' of parallelepiped\n'
                  '14 - area'
                  ' of parallelepiped')
        else:
            print('-- functions --\n'
                  '0 - back\n'
                  '1 - guide\n'
                  '2 - repeat')

    @classmethod
    def calculator_inside_menu(cls, number_of_action):
        match number_of_action:
            case 2:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' + ', str(second_number),
                      ' = ', Functionality.calculator(Action.ADD,
                                                      first_number,
                                                      second_number))
            case 3:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' - ', str(second_number),
                      ' = ', Functionality.calculator(Action.SUBTRACT,
                                                      first_number,
                                                      second_number))
            case 4:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' * ', str(second_number),
                      ' = ', Functionality.calculator(Action.MULTIPLY,
                                                      first_number,
                                                      second_number))
            case 5:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' / ', str(second_number),
                      ' = ', Functionality.calculator(Action.DIVIDE,
                                                      first_number,
                                                      second_number))
            case 6:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' % ', str(second_number),
                      ' = ', Functionality.calculator(Action.MODULO,
                                                      first_number,
                                                      second_number))
            case 7:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' // ', str(second_number),
                      ' = ', Functionality.calculator(Action.
                                                      DIVIDE_WITHOUT_REMAINDER,
                                                      first_number,
                                                      second_number))
            case 8:
                first_number, second_number \
                    = cls.user_input(2)
                print(str(first_number), ' to the power of ', str(second_number),
                      ' = ', Functionality.calculator(Action.POW,
                                                      first_number,
                                                      second_number))
            case 9:
                first_number = cls.user_input(1)
                print('sin of ', str(first_number),
                      ' = ', Functionality.calculator(Action.SIN,
                                                      first_number))
            case 10:
                first_number = cls.user_input(1)
                print('cos of ', str(first_number),
                      ' = ', Functionality.calculator(Action.COS,
                                                      first_number))
            case 11:
                first_number = cls.user_input(1)
                print('root of ', str(first_number),
                      ' = ', Functionality.calculator(Action.SQRT,
                                                      first_number))
            case 12:
                first_number = cls.input_int(cls.user_input(1))
                print('factorial of ', str(first_number),
                      ' = ', Functionality.calculator(Action.FACTORIAL,
                                                      first_number))
            case 13:
                first_number, second_number, third_number \
                    = cls.user_input(3)
                print('Perimeter of parallelepiped\n'
                      '\twith parameters ',
                      str(first_number), ', ', str(second_number),
                      ', ', str(third_number), ' is ',
                      Functionality.calculator(Action.WITH_PARALLELEPIPED,
                                               first_number,
                                               second_number,
                                               third_number, True))
            case 14:
                first_number, second_number, third_number \
                    = cls.user_input(3)
                print('Area of parallelepiped\n'
                      '\twith parameters ',
                      str(first_number), ', ', str(second_number),
                      ', ', str(third_number), ' is ',
                      Functionality.calculator(Action.WITH_PARALLELEPIPED,
                                               first_number,
                                               second_number,
                                               third_number, False))
        while True:
            print('enter number of next action'
                  ' (1 - guide):', end=' ')
            number_of_next_action = cls.input_int(input())
            if number_of_next_action == 0:
                print("\033[H\033[J")
                cls.back(True)
                break
            elif number_of_next_action == 1:
                print("\033[H\033[J")
                cls.guide(False, Position.INSIDE)
            elif number_of_next_action == 2:
                print("\033[H\033[J")
                cls.calculator_inside_menu(number_of_action)
                break
            else:
                print("\033[H\033[J")
                print('there is no such number of action')

    @classmethod
    def calculator_outside_menu(cls):
        while True:
            print('-- enter a number of action (1 - guide) --')
            action_number = cls.input_int(input())
            match action_number:
                case 0:
                    print("\033[H\033[J")
                    cls.back()
                    break
                case 1:
                    print("\033[H\033[J")
                    cls.guide(False, Position.OUTSIDE)
                case 2:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(2)
                    break
                case 3:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(3)
                    break
                case 4:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(4)
                    break
                case 5:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(5)
                    break
                case 6:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(6)
                    break
                case 7:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(7)
                    break
                case 8:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(8)
                    break
                case 9:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(9)
                    break
                case 10:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(10)
                    break
                case 11:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(11)
                    break
                case 12:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(12)
                    break
                case 13:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(13)
                    break
                case 14:
                    print("\033[H\033[J")
                    cls.calculator_inside_menu(14)
                    break
                case _:
                    print("\033[H\033[J")
                    print('there is no such number of action')

    @classmethod
    def backbone_other_menu(cls, number_of_function):
        number_of_function = cls.input_int(number_of_function)
        if number_of_function == 2:
            cls.count_in_line_menu()
        elif number_of_function == 3:
            cls.matrix_menu()
        while True:
            print('enter number of next action'
                  ' (1 - guide):', end=' ')
            number_of_next_action = cls.input_int(input())
            if number_of_next_action == 0:
                print("\033[H\033[J")
                cls.back()
                break
            elif number_of_next_action == 1:
                print("\033[H\033[J")
                cls.guide(False)
            elif number_of_next_action == 2:
                print("\033[H\033[J")
                cls.backbone_other_menu(number_of_function)
                break
            else:
                print("\033[H\033[J")
                print('there is '
                      'no such number of action')

    @classmethod
    def count_in_line_menu(cls):
        Functionality.count_in_line()

    @classmethod
    def matrix_menu(cls):
        print('enter count of rows: ', end='')
        user_count_rows = cls.input_int(input())
        print('enter count of columns: ', end='')
        user_count_columns = cls.input_int(input())
        print('enter begin of matrix: ', end='')
        user_begin = cls.input_float(input())
        print('enter step in matrix: ', end='')
        user_step = cls.input_float(input())
        Functionality.matrix(user_count_rows, user_count_columns,
                             user_begin, user_step)

    @classmethod
    def menu(cls):
        while True:
            print('enter a number of action,'
                  ' where 4 - guide:', end=' ')
            num_of_fun = cls.input_int(input())
            if num_of_fun == 0:
                print("\033[H\033[J")
                exit()
            elif num_of_fun == 1:
                print("\033[H\033[J")
                print('u\'ve chosen '
                      'first function')
                cls.calculator_outside_menu()
                break
            elif num_of_fun == 2:
                print("\033[H\033[J")
                print('u\'ve chosen '
                      'second function')
                cls.backbone_other_menu(2)
                break
            elif num_of_fun == 3:
                print("\033[H\033[J")
                print('u\'ve chosen '
                      'third function')
                cls.backbone_other_menu(3)
                break
            elif num_of_fun == 4:
                print("\033[H\033[J")
                cls.guide(True)
            else:
                print("\033[H\033[J")
                print('there\'s no function '
                      'under this number')
                print('re-', end='')
