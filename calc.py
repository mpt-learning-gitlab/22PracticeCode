from enum import Enum
from math import sin, cos, \
    sqrt, factorial


class Action(Enum):
    ADD = 0,
    # +
    SUBTRACT = 1,
    # -
    MULTIPLY = 2,
    # *
    DIVIDE = 3,
    # /
    MODULO = 4,
    # %
    DIVIDE_WITHOUT_REMAINDER = 5,
    # //
    POW = 6,
    # pow()
    SIN = 7,
    # math.sin()
    COS = 8,
    # math.cos()
    SQRT = 9,
    # math.sqrt()
    FACTORIAL = 10,
    # math.factorial()
    WITH_PARALLELEPIPED = 11
    # link to lambda expressions
    # with parallelepiped


class Calc:

    @staticmethod
    def calculator(action, a, b=None,
                   h=None, is_p=None):
        match action:
            case Action.ADD:
                return a + b
            case Action.SUBTRACT:
                return a - b
            case Action.MULTIPLY:
                return a * b
            case Action.DIVIDE:
                return a / b
            case Action.MODULO:
                return a % b
            case Action.DIVIDE_WITHOUT_REMAINDER:
                return a // b
            case Action.POW:
                return pow(a, b)
            case Action.SIN:
                return sin(a)
            case Action.COS:
                return cos(a)
            case Action.SQRT:
                return sqrt(a)
            case Action.FACTORIAL:
                return factorial(a)
            case Action.WITH_PARALLELEPIPED:
                if is_p:
                    return (lambda width, length, height:
                            4 * (width + length + height))(a, b, h)
                else:
                    return (lambda width, length, height:
                            2 * (width * length +
                                 width * height +
                                 length * height))(a, b, h)
        print('not correct input data')
