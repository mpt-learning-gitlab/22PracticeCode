class Letter:

    @staticmethod
    def count_in_line():
        symbols_count = spaces_count \
            = comma_count = 0
        print('enter the line: ', end='')
        line = input()
        for char in line:
            if char.isspace():
                spaces_count += 1
            elif char == ',':
                comma_count += 1
                symbols_count += 1
            elif char.isascii():
                symbols_count += 1

        print('count of symbols = ', symbols_count)
        print('count of spaces = ', spaces_count)
        print('count of commas = ', comma_count)
