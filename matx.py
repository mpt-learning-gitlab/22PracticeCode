class matx:

    @staticmethod
    def matrix(count_columns, count_rows, begin, step):
        print('matrix')

        cell = begin

        # initializing of array
        arr = [0] * count_columns
        for i in range(count_columns):
            arr[i] = [0] * count_rows

        for i in range(count_columns):
            for j in range(count_rows):
                arr[i][j] = cell
                cell += step
                print(arr[i][j], end=' ')
            print()
